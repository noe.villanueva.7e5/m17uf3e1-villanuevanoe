using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int CoinsAmount = 10;
    public TextMeshProUGUI textCoins;

    void Start()
    {
        textCoins.text = CoinsAmount.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        textCoins.text = CoinsAmount.ToString();
        if (CoinsAmount == 0)
        {
            SceneManager.LoadScene("FinalScene");
        }
    }

    public void ResetGame()
    {
        Time.timeScale = 1f;
        CoinsAmount = 10;
        SceneManager.LoadScene("Playground");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
