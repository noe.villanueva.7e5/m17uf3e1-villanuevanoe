using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    public AudioClip collectSound;
    public GameObject collectEffect;
    public bool rotate;
    public float rotationSpeed;

    void Update()
    {
        if (rotate)
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime, Space.World);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Collect();
            GameManager.CoinsAmount -= 1;
            Debug.Log(GameManager.CoinsAmount);
            SpawnManager.canSpawn = true;
            Destroy(gameObject);
        }
    }
    
    
    void Collect()
    {
        if (collectSound)
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        if (collectEffect)
            Instantiate(collectEffect, transform.position, Quaternion.identity);
    }
    
}
