using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public float raycastLenght = 20f;
    public GameObject coin;
    public static bool canSpawn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canSpawn)
        {
            Vector3 randomPosition = new Vector3(Random.Range(-11f, 20f), 0f, Random.Range(-1f, 28f));
            RaycastHit hit;
            if (Physics.Raycast(/*transform.position +*/ randomPosition, -Vector3.up, out hit, raycastLenght))
            {
                if (hit.collider.gameObject.GetComponent<MeshCollider>() != null || hit.collider.gameObject.GetComponent<BoxCollider>() != null)
                {
                    Instantiate(coin, hit.point + new Vector3(0f, 1.2f, 0f), Quaternion.identity);
                    //SpawnCoin();
                    canSpawn = false;
                }
            }
        }
    }

    void SpawnCoin()
    {
        //float xPosition = Random.Range(xMin, xMax);
        //float zPosition = Random.Range(zMin, zMax);
        Instantiate(coin, transform.position, Quaternion.identity);
        //Instantiate(coin, new Vector3(xPosition, 0.5f, zPosition), Quaternion.identity);
    }
}
